import { useState } from "react";

export const useInputs = () => {
  const [title, setTitle] = useState<string>("");
  const [desc, setDesc] = useState<string>("");
  const [author, setAuthor] = useState<string>("");
  const [priotity, setPriority] = useState<number>(-1);

  const handleTitleChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setTitle(event.target.value);
  };

  const handleDescChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setDesc(event.target.value);
  };

  const handleAuthorChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setAuthor(event.target.value);
  };

  const handlePriorityChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setPriority(+event.target.value);
  };

  return {
    handlePriorityChange,
    handleAuthorChange,
    handleDescChange,
    handleTitleChange,
    priotity,
    author,
    title,
    desc,
  };
};
