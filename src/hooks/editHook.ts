import { useState } from "react";

interface IUseEditFormProps {
  title: string;
  author: string;
  desctibtion: string;
  importance: number;
}

export const useEdit = (props: IUseEditFormProps) => {
  const [editTitle, setEditTitle] = useState<string>(props.title);
  const [editAuthor, setEditAuthor] = useState<string>(props.author);
  const [editDescribtion, setEditDescribtion] = useState<string>(
    props.desctibtion
  );
  const [editPriority, setEditPriority] = useState<number>(props.importance);
  const handleEditTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditTitle(event.target.value);
  };

  const handleEditAuthor = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditAuthor(event.target.value);
  };

  const handleEditDescribtion = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setEditDescribtion(event.target.value);
  };
  const handleEditPriority = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEditPriority(+event.target.value);
  };
  return {
    setEditTitle,
    setEditAuthor,
    setEditDescribtion,
    setEditPriority,
    editTitle,
    editAuthor,
    editDescribtion,
    editPriority,
    handleEditTitle,
    handleEditAuthor,
    handleEditDescribtion,
    handleEditPriority,
  };
};
