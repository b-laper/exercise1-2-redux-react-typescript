import React, { useCallback, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import * as counterActions from "../counter/index";
import { RootStoreType } from "../store/store";
import { bindActionCreators } from "redux";

export const useApp = () => {
  const [inputValue, setInputValue] = useState<number>(0);
  const [error, setError] = useState<boolean>(false);
  const dispatch = useDispatch();
  const counterState = useSelector(
    (state: RootStoreType) => state.counter.value
  );

  const {
    counterIncrementAction,
    counterDecrementAction,
    counterDoubleAction,
    counterAddValueAction,
    counterRemoveValueAction,
    counterMultiplyByValueAction,
    counterDivideByValueAction,
    counterResetAction,
  } = useMemo(() => bindActionCreators(counterActions, dispatch), [dispatch]);

  const handleInputValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setError(false);
    setInputValue(+event.target.value);
  };

  const validate = counterState < 500 && counterState > -500;
  const multiplyValueValidation =
    counterState * inputValue < 500 && counterState * inputValue > -500;
  const divideValueValidation =
    counterState / inputValue < 500 && counterState / inputValue > -500;

  const handleCounterIncrement = useCallback(() => {
    if (validate) {
      counterIncrementAction();
    }
  }, [counterIncrementAction, validate]);

  const handleCounterDecrement = useCallback(() => {
    if (validate) {
      counterDecrementAction();
    }
  }, [validate, counterDecrementAction]);

  const handleCounterDouble = useCallback(() => {
    if (validate) {
      counterDoubleAction();
    }
  }, [validate, counterDoubleAction]);

  const handleCounterAddValue = useCallback(() => {
    if (validate) {
      counterAddValueAction(inputValue);
    }
  }, [validate, counterAddValueAction, inputValue]);

  const handleCounterRemoveValue = useCallback(() => {
    if (validate) {
      counterRemoveValueAction(inputValue);
    }
  }, [validate, counterRemoveValueAction, inputValue]);

  const handleCounterMultiplyByValue = useCallback(() => {
    if (multiplyValueValidation) {
      counterMultiplyByValueAction(inputValue);
    }
  }, [multiplyValueValidation, counterMultiplyByValueAction, inputValue]);

  const handleCounterDivideByValue = useCallback(() => {
    if (inputValue === 0) {
      setError(true);
    }
    if (divideValueValidation) {
      counterDivideByValueAction(inputValue);
    }
  }, [inputValue, divideValueValidation, counterDivideByValueAction]);

  const handleReset = useCallback(() => {
    setError(false);
    counterResetAction();
  }, [counterResetAction]);

  return {
    counterState,
    error,
    handleInputValueChange,
    handleCounterIncrement,
    handleCounterDecrement,
    handleCounterDouble,
    handleCounterAddValue,
    handleCounterRemoveValue,
    handleCounterMultiplyByValue,
    handleCounterDivideByValue,
    handleReset,
  };
};
