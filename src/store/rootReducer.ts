import { combineReducers } from "redux";
//import basketSlice from "./basket/basketSlice";
import basketSlice from "./basket/basketSlice";
import { counterReducer } from "../reducers/counterReducer";
import { todoReducer } from "../reducers/todoReducers";

export const rootReducer = combineReducers({
  counter: counterReducer,
  todo: todoReducer,
  basket: basketSlice,
});
