import { PayloadAction } from "@reduxjs/toolkit";

export interface IRemoveProductPayload {
  id: number;
}
export type IRemoveProduct = PayloadAction<IRemoveProductPayload>;
