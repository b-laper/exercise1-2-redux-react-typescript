import { PayloadAction } from "@reduxjs/toolkit";

export interface IAddProductPayload {
  title: string;
  qty: number;
  id: string;
  price: number;
}
export type IAddProduct = PayloadAction<IAddProductPayload>;
