import { PayloadAction } from "@reduxjs/toolkit";

export interface IEditQtyPayload {
  qty: number;
  id: string;
}
export type IEditQty = PayloadAction<IEditQtyPayload>;
