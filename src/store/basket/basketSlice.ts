import { createSlice } from "@reduxjs/toolkit";
import { basketInitialState, IBasketItem } from "./basketState";
import { RootStoreType } from "../store";
import { IAddProduct } from "./actions/add-product.action";
import { IRemoveProduct } from "./actions/remove-product.action";
import { IEditQty } from "./actions/edit-qty-actions";
const basketSlice = createSlice({
  initialState: basketInitialState,
  name: "basket",
  reducers: {
    addProduct: (state, { payload }: IAddProduct) => {
      const productId = state.basket.findIndex(
        (product) => product.id === payload.id
      );
      const totalPrice = payload.qty * payload.price;
      const product = state.basket[productId];
      if (productId < 0) {
        state.basket.push({ ...payload, totalPrice });
      } else {
        product.qty += 1;
        product.totalPrice = product.qty * payload.price;
      }
    },
    removeProduct: (state, { payload }: IRemoveProduct) => {
      state.basket.splice(payload.id, 1);
    },
    editQuantityProduct: (state, { payload }: IEditQty) => {
      const productIndex = state.basket.findIndex(
        (product) => product.id === payload.id
      );

      if (productIndex < 0) {
        return;
      }
      const product: IBasketItem = {
        ...state.basket[productIndex],
        qty: payload.qty,
      };
      product.totalPrice = product.qty * product.price;
      state.basket[productIndex] = product;
    },
  },
});

export const { addProduct, removeProduct, editQuantityProduct } =
  basketSlice.actions;

export const basketStateSelector = (state: RootStoreType) => state.basket;

export default basketSlice.reducer;
