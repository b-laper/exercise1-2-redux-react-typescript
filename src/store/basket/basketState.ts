export interface IBasketItem {
  id: string;
  title: string;
  qty: number;
  price: number;
  totalPrice: number;
}

export interface IBasketState {
  basket: IBasketItem[];
  id: string;
}

export const basketInitialState: IBasketState = {
  basket: [],

  id: "basketID",
};
