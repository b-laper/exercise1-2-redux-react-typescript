import React, { useState } from "react";
import { editQuantityProduct } from "../../store/basket/basketSlice";
import { IBasketState } from "../../store/basket/basketState";
import { useAppDispatch } from "../../store/hooks/useAppDispatch";

interface ChangeQuantityProps {
  state: IBasketState;
  index: number;
}

export const ChangeQuantity = ({ state, index }: ChangeQuantityProps) => {
  const [quantity, setQuantity] = useState<number>(1);

  const dispatch = useAppDispatch();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuantity(+event.target.value);
  };

  const handleOnClick = (state: IBasketState, index: number) => {
    dispatch(
      editQuantityProduct({
        id: state.basket[index].id,
        qty: quantity,
      })
    );
  };

  return (
    <div>
      <label>Quantity:</label>
      <input
        min="1"
        type="number"
        onChange={handleInputChange}
        value={quantity}
      />
      <button onClick={() => handleOnClick(state, index)}>
        Change quantity
      </button>
    </div>
  );
};
