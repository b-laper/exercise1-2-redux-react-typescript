import { addProduct } from "../../store/basket/basketSlice";
import { IProduct, productMocks } from "../../mocks/products";
import { useAppDispatch } from "../../store/hooks/useAppDispatch";
import { BasketProducts } from "./basketProduct";
import "../../App.css";
import React, { useState } from "react";

export const ListOfProducts = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const [productQty, setProductQty] = useState<number>(1);

  const handleQtyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setProductQty(+event.target.value);
  };

  const addProductOnClick = (product: IProduct) => {
    dispatch(
      addProduct({
        id: product.id,
        title: product.title,
        price: product.price,
        qty: productQty,
      })
    );
  };

  return (
    <div className="App">
      {productMocks.map((product) => {
        return (
          <div key={product.id}>
            <h5>Title of product: {product.title}</h5>
            <p>Product id: {product.id}</p>
            <p>Product $: {product.price}</p>
            <input
              type="number"
              onChange={handleQtyChange}
              value={productQty}
              min="1"
            />
            <button onClick={() => addProductOnClick(product)}>
              Add to basket
            </button>
          </div>
        );
      })}
      <div>
        <BasketProducts />
      </div>
    </div>
  );
};
