import {
  basketStateSelector,
  editQuantityProduct,
  removeProduct,
} from "../../store/basket/basketSlice";
import { useAppSelector } from "../../store/hooks/useAppSelector";
import { useAppDispatch } from "../../store/hooks/useAppDispatch";
import { ChangeQuantity } from "./ChangeQuantity";

export const BasketProducts = (): JSX.Element => {
  const state = useAppSelector(basketStateSelector);
  const dispatch = useAppDispatch();

  return (
    <div>
      Basket:
      {state.basket.map((basket, index: number) => {
        return (
          <div key={basket.id}>
            <h5>Product title: {basket.title}</h5>
            <p>Product id: {basket.id}</p>
            <p>Price: {basket.price}</p>
            <p>Qty: {basket.qty}</p>
            <p>Total price: {basket.totalPrice}$</p>
            <button onClick={() => dispatch(removeProduct({ id: index }))}>
              Remove from basket
            </button>
            <ChangeQuantity state={state} index={index} />
          </div>
        );
      })}
    </div>
  );
};
