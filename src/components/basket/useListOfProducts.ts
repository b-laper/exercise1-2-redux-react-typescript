import { useAppSelector } from "../../store/hooks/useAppSelector";
import { RootStoreType } from "../../store/store";

export const useListOfProducts = () => {
  const productsState = useAppSelector((state: RootStoreType) => state.basket);
  //1.pnpm i @reduxjs/toolkit
  //2.store -> katalog na osobny stan,reducer,store,akcje -
  // a. enum do akcji
  //  b. folder initialState z interface stanu ICounterState
  //  c. tworzenie akcji createAction*<payload/undefined{value:number}>(type(z enum) )
  //3.tworzenie reducera createReducer* `<{value: number jesli initial state jest innego interfacu dodajemy interface}>`(initialState, (builer [callback]=> {
  // builder.addCase(akcja pod dany type akcji np increment akcji , (state,action / {payload})=> {state.value += state+1 np})
  //}))    dodajemy to do combine reducers
  //4. useDispatch -> bindActionCreator
  //
};
