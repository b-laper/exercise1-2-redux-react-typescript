import { Inputs } from "./inputs";
import { todoEditAction, todoRemoveAction } from "../todo";
import { useCallback, useState } from "react";
import { EditForm } from "./editForm";
import { todoSortByTitleAction } from "../actions/todoActions/action-todo-sortByTitle";
import { todoSortByDescribtionAction } from "../actions/todoActions/action-todo-sortByDescribtion";
import { todoSortByAuthorAction } from "../actions/todoActions/action-todo-sortByAuthor";
import { ITask } from "../reducers/todoReducers";
import { useAppDispatch } from "../store/hooks/useAppDispatch";
import { useAppSelector } from "../store/hooks/useAppSelector";

// formic , extendowanie interfacow, helper function do reducerów

const TaskComponent = (): JSX.Element => {
  const counterState = useAppSelector((state) => state.todo.tasks);
  const [edit, setEdit] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const handleEdit = useCallback(
    (index: number, value: ITask) => {
      dispatch(
        todoEditAction({
          index,
          updatedTask: {
            title: value.title,
            author: value.author,
            describtion: value.describtion,
            importance: value.importance,
          },
        })
      );
      setEdit(false);
    },
    [dispatch]
  );

  const handleDelete = useCallback(
    (id: number) => {
      dispatch(todoRemoveAction(id));
    },
    [dispatch]
  );
  const triggerEdit = () => {
    setEdit(true);
  };
  const handleSortByTitle = useCallback(() => {
    dispatch(todoSortByTitleAction());
  }, [dispatch]);
  const handleSortByAuthor = useCallback(() => {
    dispatch(todoSortByAuthorAction());
  }, [dispatch]);
  const handleSortByDescribtion = useCallback(() => {
    dispatch(todoSortByDescribtionAction());
  }, [dispatch]);
  return (
    <>
      <Inputs />

      <div className="App">
        <button onClick={handleSortByTitle}>SortByTitle</button>
        <button onClick={handleSortByAuthor}>SortByAuthor</button>
        <button onClick={handleSortByDescribtion}>SortByDescribtion</button>
        {counterState.map((task, index: number) => (
          <div key={`${task.author}${index}`}>
            <h5> Task Number {index + 1}</h5>
            <p>Author: {task.author}</p>
            <p>Title: {task.title}</p>
            <p>Describtion: {task.describtion}</p>
            <p> Priotity: {task.importance}</p>
            <button onClick={() => handleDelete(index)}>Delete task</button>
            <button onClick={triggerEdit}>Edit</button>
            {edit && (
              <>
                <EditForm
                  title={task.title}
                  author={task.author}
                  desctibtion={task.describtion}
                  importance={task.importance}
                  onClick={(value) => handleEdit(index, value)}
                />
              </>
            )}
          </div>
        ))}
      </div>
    </>
  );
};
export default TaskComponent;
