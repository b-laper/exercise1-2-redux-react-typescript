import { useCallback, useMemo } from "react";
import { useInputs } from "../hooks/taskHook";
import { bindActionCreators } from "redux";
import * as todoActions from "../todo/index";
import { useDispatch } from "react-redux";
export const Inputs = (): JSX.Element => {
  const dispatch = useDispatch();

  const { todoAddAction } = useMemo(
    () => bindActionCreators(todoActions, dispatch),
    [dispatch]
  );

  const {
    handlePriorityChange,
    handleAuthorChange,
    handleDescChange,
    handleTitleChange,
    priotity,
    author,
    title,
    desc,
  } = useInputs();

  const addTodo = useCallback(() => {
    todoAddAction({
      title,
      author,
      describtion: desc,
      importance: priotity,
    });
  }, [author, desc, priotity, title, todoAddAction]);
  return (
    <div className="App">
      <label>Task title</label>
      <input onChange={handleTitleChange} type="text" />
      <label>Task desc.</label>
      <input onChange={handleDescChange} type="text" />
      <label>Task author</label>
      <input onChange={handleAuthorChange} type="text" />
      <label>Priotity lvl</label>
      <input onChange={handlePriorityChange} type="number" />
      <button onClick={addTodo}>Add</button>
    </div>
  );
};
