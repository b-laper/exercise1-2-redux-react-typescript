import React, { useCallback, useState } from "react";
import { useEdit } from "../hooks/editHook";
import { todoEditAction } from "../todo";
import { useDispatch } from "react-redux";
import { setPriority } from "os";
import { ITask } from "../reducers/todoReducers";
interface IEditFormProps {
  title: string;
  author: string;
  desctibtion: string;
  importance: number;
  onClick: (value: ITask) => void;
}

export const EditForm = (props: IEditFormProps): JSX.Element => {
  const {
    handleEditTitle,
    handleEditAuthor,
    handleEditDescribtion,
    handleEditPriority,
    editTitle,
    editAuthor,
    editDescribtion,
    editPriority,
  } = useEdit(props);

  const value = {
    title: editTitle,
    author: editAuthor,
    describtion: editDescribtion,
    importance: editPriority,
  };

  return (
    <div>
      <label>Task Title:</label>
      <input onChange={handleEditTitle} type="text" value={editTitle} />
      <label>Task Desc.:</label>
      <input
        onChange={handleEditDescribtion}
        type="text"
        value={editDescribtion}
      />
      <label>Task Author:</label>
      <input onChange={handleEditAuthor} type="text" value={editAuthor} />
      <input onChange={handleEditPriority} type="text" value={editPriority} />

      <button onClick={() => props.onClick(value)}>UpdateTask</button>
    </div>
  );
};
