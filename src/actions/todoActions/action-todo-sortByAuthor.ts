import { todoActions } from "../../reducers/todoReducers";

export const todoSortByAuthorAction = () => {
  return {
    type: todoActions.sortByAuthor,
    payload: undefined,
  };
};
