import { ITask, todoActions } from "../../reducers/todoReducers";

export interface IeditPayload {
  updatedTask: ITask;
  index: number;
}

export const todoEditAction = (payload: IeditPayload) => {
  return {
    type: todoActions.editTodo,
    payload: payload,
  };
};
