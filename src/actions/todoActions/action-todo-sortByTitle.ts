import { todoActions } from "../../reducers/todoReducers";

export const todoSortByTitleAction = () => {
  return {
    type: todoActions.sortByTitle,
    payload: undefined,
  };
};
