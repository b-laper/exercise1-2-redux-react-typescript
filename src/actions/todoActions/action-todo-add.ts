import { ITask, todoActions } from "../../reducers/todoReducers";
import { IAction } from "../actions";
import { TodoActionTypesEnum } from "./action-todo-enum";

export interface ItodoAddAction
  extends IAction<typeof TodoActionTypesEnum.ADDTODO, ITask> {}

export const todoAddAction = (payload: ITask) => {
  return {
    type: todoActions.addTodo,
    payload: payload,
  };
};
