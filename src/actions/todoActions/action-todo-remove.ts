import { todoActions } from "../../reducers/todoReducers";

export const todoRemoveAction = (payload: number) => {
  return {
    type: todoActions.removeTodo,
    payload: payload,
  };
};
