import { actions } from "../../reducers/counterReducer";

export const counterAddValueAction = (value: number) => {
  return {
    type: actions.add_value,
    payload: value,
  };
};
