import { actions } from "../../reducers/counterReducer";

export const counterRemoveValueAction = (value: number) => {
  return {
    type: actions.remove_value,
    payload: value,
  };
};
