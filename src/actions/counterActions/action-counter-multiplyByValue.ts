import { actions } from "../../reducers/counterReducer";

export const counterMultiplyByValueAction = (value: number) => {
  return {
    type: actions.multiply_by_value,
    payload: value,
  };
};
