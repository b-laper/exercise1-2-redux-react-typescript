import { actions } from "../../reducers/counterReducer";

export const counterDecrementAction = () => {
  return {
    type: actions.decrement,
    payload: undefined,
  };
};
