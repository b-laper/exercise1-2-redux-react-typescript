import { actions } from "../../reducers/counterReducer";

export const counterIncrementAction = () => {
  return {
    type: actions.increment,
    payload: undefined,
  };
};
