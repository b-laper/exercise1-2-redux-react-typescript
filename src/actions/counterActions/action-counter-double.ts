import { actions } from "../../reducers/counterReducer";

export const counterDoubleAction = () => {
  return {
    type: actions.double,
    payload: undefined,
  };
};
