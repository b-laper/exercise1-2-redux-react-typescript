import { actions } from "../../reducers/counterReducer";

export const counterResetAction = () => {
  return {
    type: actions.reset,
    payload: undefined,
  };
};
