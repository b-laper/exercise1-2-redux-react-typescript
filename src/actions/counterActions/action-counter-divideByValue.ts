import { actions } from "../../reducers/counterReducer";

export const counterDivideByValueAction = (value: number) => {
  return {
    type: actions.divide_by_value,
    payload: value,
  };
};
