import { createAction, createReducer } from "@reduxjs/toolkit";
import { IeditPayload } from "../actions/todoActions/action-todo-edit";
// import { ItodoAddAction } from "../actions/todoActions/action-todo-add";

// import { TodoActionTypesEnum } from "../actions/todoActions/action-todo-enum";
// import { ItodoRemoveAction } from "../actions/todoActions/action-todo-remove";
// import { ItodoSortByAuthorAction } from "../actions/todoActions/action-todo-sortByAuthor";
// import { ItodoSortByDescribtion } from "../actions/todoActions/action-todo-sortByDescribtion";
// import { ItodoSortByTitleAction } from "../actions/todoActions/action-todo-sortByTitle";
// import { ICounterState } from "./counterReducer";

export interface ITask {
  title: string;
  describtion: string;
  author: string;
  importance: number;
}

export interface ITodoState {
  tasks: ITask[];
}

const initialState: ITodoState = {
  tasks: [
    {
      title: "Task 1",
      describtion: "big task",
      author: "Human",
      importance: 5,
    },
  ],
};

// type Action =
//   | ItodoAddAction
//   | ItodoRemoveAction
//   | ItodoEditAction
//   | ItodoSortByTitleAction
//   | ItodoSortByAuthorAction
//   | ItodoSortByDescribtion;

export const todoActions = {
  addTodo: createAction<ITask>("todo/addTodo"),
  removeTodo: createAction<number>("todo/removeTodo"),
  editTodo: createAction<IeditPayload>("todo/editTodo"),
  sortByTitle: createAction("todo/sortByTitle"),
  sortByAuthor: createAction("todo/sortByAuthor"),
  sortByDescribtion: createAction("todo/sortByDescribtion"),
};

// export const todoReducers = (
//   state: ITodoState = initialState,
//   action: Action
// ) => {
//   switch (action.type) {
//     case TodoActionTypesEnum.ADDTODO:
//       return { ...state, tasks: [...state.tasks, action.payload] };
//     case TodoActionTypesEnum.REMOVETODO:
//       return {
//         ...state,
//         tasks: state.tasks.filter((_item, index) => index !== action.payload),
//       };
//     case TodoActionTypesEnum.EDITTODO:
//       return {
//         tasks: state.tasks.map((task, index) => {
//           if (action.payload.index === index) {
//             return action.payload.updatedTask;
//           }
//           return task;
//         }),
//       };
//     case TodoActionTypesEnum.SORTBYVALUE:
//       return {
//         tasks: state.tasks.slice().sort((a, b) => {
//           let nameA = a.title.toLowerCase(),
//             nameB = b.title.toLowerCase();
//           if (nameA < nameB) return -1;
//           if (nameA > nameB) return 1;
//           return 0;
//         }),
//       };
//     case TodoActionTypesEnum.SORTBYAUTHOR:
//       return {
//         tasks: state.tasks.slice().sort((a, b) => {
//           let nameA = a.author.toLowerCase(),
//             nameB = b.author.toLowerCase();
//           if (nameA < nameB) return -1;
//           if (nameA > nameB) return 1;
//           return 0;
//         }),
//       };
//     case TodoActionTypesEnum.SORTBYDESCRIBTION:
//       return {
//         tasks: state.tasks.slice().sort((a, b) => {
//           let nameA = a.describtion.toLowerCase(),
//             nameB = b.describtion.toLowerCase();
//           if (nameA < nameB) return -1;
//           if (nameA > nameB) return 1;
//           return 0;
//         }),
//       };
//     default:
//       return state;
//   }
// };
export const todoReducer = createReducer<ITodoState>(
  initialState,
  (builder) => {
    builder
      .addCase(todoActions.addTodo, (state, action) => {
        state.tasks.push(action.payload);
      })
      .addCase(todoActions.removeTodo, (state, action) => {
        state.tasks.splice(action.payload, 1);
      })
      .addCase(todoActions.editTodo, (state, action) => {
        return {
          tasks: state.tasks.map((task, index) => {
            if (action.payload.index === index) {
              return action.payload.updatedTask;
            }
            return task;
          }),
        };
      })
      .addCase(todoActions.sortByTitle, (state, _action) => {
        return {
          tasks: state.tasks.slice().sort((a, b) => {
            let nameA = a.title.toLowerCase(),
              nameB = b.title.toLowerCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
          }),
        };
      })
      .addCase(todoActions.sortByAuthor, (state, _action) => {
        return {
          tasks: state.tasks.slice().sort((a, b) => {
            let nameA = a.author.toLowerCase(),
              nameB = b.author.toLowerCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
          }),
        };
      })
      .addCase(todoActions.sortByDescribtion, (state, action) => {
        return {
          tasks: state.tasks.slice().sort((a, b) => {
            let nameA = a.describtion.toLowerCase(),
              nameB = b.describtion.toLowerCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
          }),
        };
      })
      .addDefaultCase((state) => state);
  }
);
