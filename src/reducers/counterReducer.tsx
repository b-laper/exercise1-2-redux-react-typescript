// import { ICounterAddValueAction } from "../actions/counterActions/action-counter-addValue";
// import { ICounterDecrementAction } from "../actions/counterActions/action-counter-decrement";
// import { ICounterDivideByValueAction } from "../actions/counterActions/action-counter-divideByValue";
// import { ICounterDoubleAction } from "../actions/counterActions/action-counter-double";
// import { ICounterIncrementAction } from "../actions/counterActions/action-counter-increment";
// import { ICounterMultiplyByValueAction } from "../actions/counterActions/action-counter-multiplyByValue";
// import { ICounterRemoveValueAction } from "../actions/counterActions/action-counter-removeValue";
// import { ICounterResetAction } from "../actions/counterActions/action-counter-reset";
// import { CounterActionTypesEnum } from "../actions/action-types.enum";
import { createAction, createReducer } from "@reduxjs/toolkit";

export interface ICounterState {
  value: number;
}

const initialState: ICounterState = {
  value: 0,
};

// type ICounterActions =
//   | ICounterIncrementAction
//   | ICounterDecrementAction
//   | ICounterDoubleAction
//   | ICounterAddValueAction
//   | ICounterRemoveValueAction
//   | ICounterMultiplyByValueAction
//   | ICounterDivideByValueAction
//   | ICounterResetAction;

// export const counterReducers = (
//   state: ICounterState = initialState,
//   action: ICounterActions
// ): ICounterState => {
//   switch (action.type) {
//     case CounterActionTypesEnum.INCREMENT:
//       return { value: state.value + 1 };
//     case CounterActionTypesEnum.DECREMENT:
//       return { value: state.value - 1 };
//     case CounterActionTypesEnum.DOUBLE:
//       return { value: state.value * 2 };
//     case CounterActionTypesEnum.ADD_VALUE:
//       return { value: state.value + action.payload };
//     case CounterActionTypesEnum.REMOVE_VALUE:
//       return { value: state.value - action.payload };
//     case CounterActionTypesEnum.MULTIPLY_BY_VALUE:
//       return { value: state.value * action.payload };
//     case CounterActionTypesEnum.DIVIDE_BY_VALUE:
//       return { value: +(state.value / action.payload).toFixed(2) };
//     case CounterActionTypesEnum.RESET:
//       return { value: 0 };
//     default:
//       return state;
//   }
// };

export const actions = {
  increment: createAction("counter/increment"),
  decrement: createAction("counter/decrement"),
  double: createAction("counter/double"),
  add_value: createAction<number>("counter/add_value"),
  remove_value: createAction<number>("counter/remove_value"),
  multiply_by_value: createAction<number>("counter/multiply_by_value"),
  divide_by_value: createAction<number>("counter/divide_by_value"),
  reset: createAction("counter/reset"),
};

export const counterReducer = createReducer<ICounterState>(
  initialState,
  (builder) => {
    builder
      .addCase(actions.increment, (state, _action) => {
        state.value++;
      })
      .addCase(actions.decrement, (state, _action) => {
        state.value--;
      })
      .addCase(actions.double, (state, _action) => {
        state.value = state.value * 2;
      })
      .addCase(actions.add_value, (state, action) => {
        state.value += action.payload;
      })
      .addCase(actions.remove_value, (state, action) => {
        state.value -= action.payload;
      })
      .addCase(actions.multiply_by_value, (state, action) => {
        state.value *= action.payload;
      })
      .addCase(actions.divide_by_value, (state, action) => {
        (state.value /= action.payload).toFixed(2);
      })
      .addCase(actions.reset, (state, _action) => {
        state.value = 0;
      })
      .addDefaultCase((state, _action) => {
        return state;
      });
  }
);
