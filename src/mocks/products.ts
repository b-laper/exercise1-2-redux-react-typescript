export interface IProduct {
  id: string;
  title: string;
  price: number;
}

export const productMocks: IProduct[] = [
  { id: "1", price: 20, title: "Pierwszy" },
  { id: "2", price: 10, title: "Drugi" },
  { id: "3", price: 30, title: "Trzeci" },
  { id: "4", price: 80.9, title: "Czwarty" },
];
