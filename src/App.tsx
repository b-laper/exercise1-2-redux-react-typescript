import TaskComponent from "./components/taskComponent";
import { useApp } from "./hooks/appHook";
import "./App.css";
import { ListOfProducts } from "./components/basket/listOfProducts";
function App() {
  const {
    counterState,
    error,
    handleInputValueChange,
    handleCounterIncrement,
    handleCounterDecrement,
    handleCounterDouble,
    handleCounterAddValue,
    handleCounterRemoveValue,
    handleCounterMultiplyByValue,
    handleCounterDivideByValue,
    handleReset,
  } = useApp();

  // return (
  //   <div>
  //     <ListOfProducts />
  //   </div>
  // );

  return (
    <>
      <div className="App">
        Counter: {counterState}
        <input
          onChange={handleInputValueChange}
          type="number"
          placeholder="Enter number"
        />
        <div>
          <div>
            <button onClick={handleReset}>RESET</button>
          </div>
          <button onClick={handleCounterIncrement}>+1</button>
          <button onClick={handleCounterDecrement}>-1</button>
          <button onClick={handleCounterDouble}>x2</button>
          <div>
            <button onClick={handleCounterAddValue}>+value</button>
            <button onClick={handleCounterRemoveValue}>-value</button>
          </div>
          <div>
            <button onClick={handleCounterMultiplyByValue}>Multiply</button>
            <button onClick={handleCounterDivideByValue}>Divide</button>
            {error && <p>Don't divide by 0!</p>}
          </div>
        </div>
      </div>
      <div>
        <TaskComponent />
      </div>
    </>
  );
}

export default App;
